// import protobuf = require("../protobuf/protobuf");

import { isSleeping } from "./Helper";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Helloworld extends cc.Component {

    @property(cc.RigidBody3D)
    readonly ball: cc.RigidBody3D = null;
    @property(cc.Vec3)
    private velocity: cc.Vec3 = cc.v3(88, 0, 0);

    private z = 1;
    private stepEnable = false;
    private stepTotal = 0;
    private shootCount = 0;

    public changeScene(evt, scene) {
        if (scene == null || scene == "") return;
        cc.director.loadScene(scene);
    }

    start() {        
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
    }

    private onKeyDown(evt) {
        if (evt.keyCode == cc.macro.KEY.space)//13)
        {
            this.shoot();
        }
    }
    public shoot() {
        if (isSleeping()) {
            // window['CANNON']['Persistence'].times++;
            this.shoot2();

            // 启动手动刷新
            this.stepEnable = true;

            // 清除上一次击球花费的步数
            this.stepTotal = 0;
            this.shootCount += 1;
        }
    }
    public shoot2() {
        if (this.ball == null) return;

        if (this.velocity.z > 15) {
            this.z = -3;
        } else if (this.velocity.z < -15) {
            this.z = 3;
        }
        this.velocity.z += this.z;
        this.ball.setLinearVelocity(this.velocity);
    }

    hashCode(str) {
        //获取字符串的 哈希值 
        // str = str.toLowerCase();

        var hash = 1315423911, i, ch;
        for (i = str.length - 1; i >= 0; i--) {
            ch = str.charCodeAt(i);
            hash ^= ((hash << 5) + ch + (hash >> 2));
        }
        return (hash & 0x7FFFFFFF).toString();
    }

    public update(dt) {
        if (!this.stepEnable) {
            if (isSleeping()) {
                const p3dm = cc.director.getPhysics3DManager();
                p3dm.enabled = false;
            }
            return;
        }

        const p3dm = cc.director.getPhysics3DManager();
        p3dm.enabled = true;
        p3dm.update(dt)
        p3dm.enabled = false;

        this.stepTotal = this.stepTotal + 1;
        if (isSleeping()) {
            this.stepEnable = false;
            var sunStr = (p3dm.physicsWorld.world.time).toString();

            var bodys = p3dm.physicsWorld.world.bodies;                      
            bodys.forEach((body) => {
                // console.log(body.node.name, body.body.position);
                sunStr += body.position.x;
                sunStr += body.position.y;
                sunStr += body.position.z;
            })

            var hash = this.hashCode(sunStr);
            var oldHash = localStorage.getItem("s-" + this.shootCount);
            if (oldHash == hash) {
                console.log(`[${this.shootCount}] ` + hash);
            }
            else if (oldHash) {
                console.log(`[${this.shootCount}]* ${oldHash}`);
                localStorage.setItem("s-" + this.shootCount, hash);
            }
            else {
                localStorage.setItem("s-" + this.shootCount, hash);
            }
            console.log("-> ", p3dm.physicsWorld.world.time + ", " + this.stepTotal, ", " + this.hashCode(sunStr));
        }
        /*
        else {
            var bodys = p3dm.physicsWorld.world.bodies;                      
            bodys.forEach((body) => {
                console.log(this.stepTotal + ", " + body.timeLastSleepy + ", " + body.angularVelocity +  ", " + body.position.x + ", " + body.position.y + ", " + body.position.z);
            })
        }
        */
    }

}
