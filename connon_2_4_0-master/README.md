# hello-world

Hello world new project template.

## 配置说明

![config](/imgs/config.jpg)

模式选项：

- 模式 0，使用系统的 cos \ sin
- 模式 1，使用预计算的 cos \ sin，角度的小数位由后面的数字决定
- 模式 2，使用系统的 cos \ sin，返回值的小数位由后面的数字决定

小数位长度：

    表示位数。模式 1 时建议设置成 1；模式 2 时建议设置成 7；

---

useFixedTime：

    表示允许执行多步模拟，建议保持为 false，可以通过 maxSubStep 控制最多步进次数

useAccumulator:

    表示使用物理框架实现的非插值多步模拟，建议保持为 true (false 表示使用插值多步模拟，可能会导致物理事件不连续)

## 注意事项

- 物理系统的 deltaTime 过大或物理帧率过低，并且刚体速度过快时，容易出现穿透或嵌入过深才产生碰撞的情况。

- 如果要保证不同设备在花费同样时间时，物理模拟是一致的，哪么帧率低的设备需要增大 maxSubStep ，建议设置成一个很大的数字，比如 100。

## 测试数据

- 测试参数：

| 物理模拟帧率 | 允许自动休眠 | 最多步进次数 | -   | 模式选项 | 小数位长度 |
| ------------ | ------------ | ------------ | --- | -------- | ---------- |
| `60`         | `false`      | `1`          | -   | `0`      | `1`        |

- 截屏对比

iphone 6, 27 FPS:

![iphone-6](/imgs/iphone-6.png)

xiaomi 8, 56 FPS:

![xiaomi-8](/imgs/xiaomi-8.jpg)

oppo r11t, 47 FPS:

![oppo-r11t](/imgs/oppo-r11t.png)
