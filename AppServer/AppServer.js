const ws = require('nodejs-websocket');
const { ServerCmd } = require('./SCmd');
const SCmd = require("./SCmd");

var usernameList = [];
var moveData = {};
var myConnection = null;

let server = ws.createServer(connection => {
    myConnection = connection;
    connection.on('text', function(textValue) {
        var _textObj = JSON.parse(textValue);
        if (_textObj.data) {
            var _data = JSON.parse(_textObj.data);
        }

        console.log(`c2s: ${_textObj.command} , `, _data);
        
        switch (_textObj.command) {
            case SCmd.ClientCmd.USER_LOGIN:
                let username = _data.username;
                if (usernameList.indexOf(username) == -1)
                {
                    usernameList.push(username);
                    broadcast(SCmd.ServerCmd.USER_LOGIN_SUCCESS, _data);
                }
                else{
                    broadcast(SCmd.ServerCmd.USER_LOGIN_FAIL, _data);
                }
                break;
            case SCmd.ClientCmd.BALL_MOVE:
                moveData[_data.username] = _data.key;
                break;
        }
    });

    // connection.on('close', function(code){
    //     console.log('code : ', code);

    //     broadcast(SCmd.ServerCmd.CLOSE_CONNECT,{});
    // });
})


const sendToClient = (command, data)=>{
    console.log(`s2c: ${command} , ${data}`);
    var _clientObj = {};
    _clientObj.command = command;
    if (data) _clientObj.data = JSON.stringify(data);
    myConnection.sendText(JSON.stringify(_clientObj))
}

const broadcast = (command, data) => {
    server.connections.forEach((conn) =>{
        var _clientObj = {
            "command": command,
            "data": JSON.stringify(data)
        }
        conn.sendText(JSON.stringify(_clientObj))
        // console.log(`s2c: broadcast, ${command} , ${data}`);
    })
}

let frameCount = 0;
let updateFrame =function(){
    frameCount++;

    Object.keys(moveData).forEach(function(username){
        console.log(`${frameCount}, ${username}, ${moveData[username]}`);
    });

    broadcast(SCmd.ServerCmd.UPDATE_FRAME, { moveData : moveData, frameCount : frameCount })
    
    moveData = {};
}
let myInterval=setInterval(updateFrame,30);

server.listen(8002);

console.log("Listen 8002");