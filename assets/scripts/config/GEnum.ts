var GameEnum = {
        pageFlag: cc.Enum({
            common_Problem: 0,
            tchnical_Proposal: 1,
            tools_Techniques: 2
        }),
        level: cc.Enum({
            normal: 0,
            hight: 1
        }),
        viewType: cc.Enum({
            icon: 0,
            table: 1
        }),
        userActionType: cc.Enum({
            register: 0,
            login: 1
        })
}
export default GameEnum;